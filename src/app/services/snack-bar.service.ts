import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class SnackBarService {

  private durationDefault = 3;

  constructor(private snackBar: MatSnackBar) { }

public showSnackBar(message: string, action: string, durationPrm?: number){
    this.snackBar.open(message, action,
      {
        duration: durationPrm != null ?durationPrm * 1000 :this.durationDefault * 1000
      }
    );
  }
}
