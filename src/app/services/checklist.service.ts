import { Injectable } from '@angular/core';
import { CheckListItem } from '../_models/checklist_item';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environments';

@Injectable({
  providedIn: 'root'
})
export class ChecklistService {

  constructor(private httpClient: HttpClient) { }

  public getAllCheckListItem(): Observable<CheckListItem[]>{

    return this.httpClient.get<CheckListItem[]>(`${environment.apiBaseEndpointUrl}/checklist-items`);

  }

  public saveCheckListItem(checkListItem: CheckListItem): Observable<CheckListItem>{
    console.log("***** ->" + JSON.stringify(checkListItem));
    return this.httpClient.post<CheckListItem>(`${environment.apiBaseEndpointUrl}/checklist-items`, checkListItem);
  }

  public updateCheckListItem(checkListItem: CheckListItem): Observable<CheckListItem>{
    return this.httpClient.put<CheckListItem>(`${environment.apiBaseEndpointUrl}/checklist-items`, checkListItem);
  }

  public deleteCheckListItem(guid: string): Observable<void>{
    return this.httpClient.delete<void>(`${environment.apiBaseEndpointUrl}/checklist-items/${guid}` );
  }

  public updateStatusCheckListItem(guid: string, status: boolean): Observable<void>{
    return this.httpClient.patch<void>(`${environment.apiBaseEndpointUrl}/checklist-items/${guid}`, { "isCompleted": status } );
  }

}
