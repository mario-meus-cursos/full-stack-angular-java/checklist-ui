import { HttpClient } from '@angular/common/http';
import { Category } from './../_models/category';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environments';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private httpClient: HttpClient) { }

  public getAllCategories(): Observable<Category[]>{

    return this.httpClient.get<Category[]>(`${environment.apiBaseEndpointUrl}/categories`);

  }

  public saveCategory(name: Category): Observable<void>{
    return this.httpClient.post<void>(`${environment.apiBaseEndpointUrl}/categories`, {name});
  }

  public updateCategory(category: Category): Observable<Category>{
    console.log("edit..." + category)
    return this.httpClient.put<Category>(`${environment.apiBaseEndpointUrl}/categories`, category);
  }

  public deleteCategory(guid: string): Observable<void>{
    return this.httpClient.delete<void>(`${environment.apiBaseEndpointUrl}/categories/${guid}` );
  }
}
