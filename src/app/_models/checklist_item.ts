import { Category } from "./category";

export class CheckListItem{
  public guid!: string;
  public description!: string;
  public deadline!: Date;
  public postDate!: Date;
  public isCompleted!: boolean;
  public category!: Category;
}
