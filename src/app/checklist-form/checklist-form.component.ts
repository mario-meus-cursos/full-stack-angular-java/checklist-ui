import { ChecklistService } from './../services/checklist.service';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { CheckListItem } from '../_models/checklist_item';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { Category } from '../_models/category';
import { CategoryService } from '../services/category.service';
import { SnackBarService } from '../services/snack-bar.service';

@Component({
  selector: 'app-checklist-form',
  templateUrl: './checklist-form.component.html',
  styleUrls: ['./checklist-form.component.css']
})
export class ChecklistFormComponent implements OnInit{

  @Input() public actionName =  'Editar';
  @Input() public editableCheckList!: CheckListItem;
  @Output() public formCloseEvent: EventEmitter<boolean> = new EventEmitter<boolean>;

  @ViewChild(`checkListFormFormDirective`) public checkListFormFormDirective!: FormGroupDirective;

  public checkListForm! : FormGroup;

  public categories: Category[] = [];


  constructor (private formBuilder: FormBuilder, private categoryService: CategoryService,
    private checklistService: ChecklistService, private snackBarService: SnackBarService ){}


  ngOnInit(): void {
    this.categoryService.getAllCategories().subscribe(
      (resp: Category[]) => {
        this.categories = resp;
        this.createForm();
      },(err: any) => {
        console.log(`erro em consumir a api ${err}`);
      }
    )
  }

  public compareCategories(categoryOne: Category, categoryTwo: Category): boolean {

    return (categoryOne != null && categoryTwo != null) &&
      (categoryOne.guid ==  categoryTwo.guid) &&
      (categoryOne.name ==  categoryTwo.name);

  }
  private createForm(){
    this.checkListForm = this.formBuilder.group({
      description: [this.editableCheckList != null ? this.editableCheckList.description : '', Validators.required],
      deadline: [this.editableCheckList != null ? this.editableCheckList.deadline : new Date() , Validators.required],
      isCompleted: [this.editableCheckList != null ? this.editableCheckList.isCompleted : false, Validators.required],
      category: [this.editableCheckList != null ? this.editableCheckList.category : null, Validators.required]
    });

  }


  public clearForm(){
    this.checkListForm.reset();
  }

  public cancel(){
    this.formCloseEvent.emit(false);
  }

  public save(){

    if (this.actionName == 'Editar'){
      var updateCheckListItem = {
        guid : this.editableCheckList.guid,
        description: this.checkListForm.value['description'],
        deadline:  this.checkListForm.value['deadline'],
        isCompleted: this.checkListForm.value['isCompleted'],
        categoryGuid: this.checkListForm.value['category']['guid']
      }

      this.checklistService.updateCheckListItem(updateCheckListItem as any).subscribe(
        (resp: any) =>{
          this.formCloseEvent.emit(true);
        //  this.clearForm();

        },(err: any) => {
          this.snackBarService.showSnackBar("Não foi possivel atualizar ochecklist. Tente novamente. ", "OK");

        }
      )

    }else{

      var createCheckListItem = {
        description: this.checkListForm.value['description'],
        deadline: this.checkListForm.value['deadline'],
        isCompleted: this.checkListForm.value['isCompleted'],
        categoryGuid: this.checkListForm.value['category']['guid']
      }

      this.checklistService.saveCheckListItem(createCheckListItem as any).subscribe(
        (resp: any) => {
          this.formCloseEvent.emit(true);
        //  this.clearForm();

        },(err: any) =>{
          this.snackBarService.showSnackBar("Não foi possivel criar ochecklist. Tente novamente.", "OK");
        }
      )

    }

   // this.clearForm();
   // this.formCloseEvent.emit(true);
  }
}
