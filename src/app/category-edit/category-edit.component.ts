import { Category } from './../_models/category';
import { Component, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';



@Component({
  selector: 'app-category-edit',
  templateUrl: './category-edit.component.html',
  styleUrls: ['./category-edit.component.css']
})
export class CategoryEditComponent {

  public editableCategory!: Category;
  public actionName: string = 'Editar';

  constructor(public dialogRef: MatDialogRef<CategoryEditComponent>,  @Inject(MAT_DIALOG_DATA) dialogData: any ){

      if(dialogData.editableCategory != null){
        this.editableCategory = dialogData.editableCategory;
      }

      if(dialogData.actionName != null){
        this.actionName = dialogData.actionName;
      }
  }

  public closeModelWindow($event: any){

    console.log($event);

    if($event != null) this.dialogRef.close($event);

  }

}
