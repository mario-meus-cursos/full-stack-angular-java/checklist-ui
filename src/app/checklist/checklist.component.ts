import { ChecklistService } from './../services/checklist.service';
import { Category } from './../_models/category';
import { ChecklistEditComponent } from './../checklist-edit/checklist-edit.component';
import { Component, OnInit } from '@angular/core';
import { CheckListItem } from '../_models/checklist_item';
import { DialogComponent } from '../dialog/dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { CategoryService } from '../services/category.service';
import { SnackBarService } from '../services/snack-bar.service';



@Component({
  selector: 'app-checklist',
  templateUrl: './checklist.component.html',
  styleUrls: ['./checklist.component.css']
})
export class ChecklistComponent implements OnInit {

  public displayedColumns: string[] = ['id', 'description', 'deadline', 'postDate', 'isCompleted', 'category' ,'actions'];
  public dataSource: CheckListItem[] = [];

  constructor(private dialog : MatDialog ,
    private checklistService :ChecklistService,
    private snackBarService: SnackBarService){

  }

  ngOnInit(): void {

    this.loadAllItems();
  }

  private loadAllItems(){

    this.checklistService.getAllCheckListItem().subscribe(
      (resp: CheckListItem[]) => {this.dataSource = resp;}
     );
  }
  public createNewItem(){

    this.dialog.open(ChecklistEditComponent , { disableClose: true,
      data : { actionName : 'Criar' }
    }).afterClosed().subscribe(
      resp => {
        if(resp){
          this.loadAllItems();
          this.snackBarService.showSnackBar('Checklist Criado com sucesso.', 'OK');
        }
      }
    );
  }

  public updateCompletedStatus(guid: string, status : boolean){
    this.dialog.open(DialogComponent , {disableClose: true,
      data : {dialogMsg: `Confirma a alteração do Status da tarefa?`, leftButtonLabel: 'Não', rightButtonLabel: 'Sim'}}).afterClosed().subscribe(
      resp => {
        if(resp) {

          this.checklistService.updateStatusCheckListItem(guid, status).subscribe(
            (resp: any) =>{
              this.loadAllItems();
              this.snackBarService.showSnackBar('Status da tarefa alterado.', 'OK');
            },(err: any) => {

              this.snackBarService.showSnackBar('Não foi possivel alterar o Status da tarefa, tente novamente.', 'OK');

            }
          )
        }
      }
    );
  }

  public editCheckList(inputChecklist : CheckListItem){

    this.dialog.open(ChecklistEditComponent , { disableClose: true,
      data : { editableCheckList : inputChecklist, actionName : 'Editar' }
    }).afterClosed().subscribe(
      resp => {
        if (resp){
          this.loadAllItems();
          this.snackBarService.showSnackBar('Checklist alterado com sucesso.', 'OK')
        }

      }
    );
  }

  public deleteCheckList(checklistItem : CheckListItem){

    this.dialog.open(DialogComponent , {disableClose: true,
      data : {dialogMsg: `Deseja Excluir o checklist `+ checklistItem.description + `?`, leftButtonLabel: 'Desistir', rightButtonLabel: 'Confirma'}}).afterClosed().subscribe(
      resp => {
        if(resp) {
          this.checklistService.deleteCheckListItem(checklistItem.guid).subscribe(
            (resp: any) => {
              this.loadAllItems();
              this.snackBarService.showSnackBar('Checklist apagada com sucesso.', 'OK')

            },(err: any) =>{
              this.snackBarService.showSnackBar('Não foi possivel apaguar o checkList, tente novamente', 'OK')
            }

          )
        }
      }
    );
  }

}
