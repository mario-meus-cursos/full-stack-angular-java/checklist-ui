import { SnackBarService } from './../services/snack-bar.service';
import { CategoryService } from './../services/category.service';
import { DialogComponent } from './../dialog/dialog.component';
import { Component, OnInit } from '@angular/core';
import { Category } from '../_models/category';
import { MatDialog } from '@angular/material/dialog';
import { CategoryEditComponent } from '../category-edit/category-edit.component';


@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit{

  public displayedColumns: string[] = ['id', 'name', 'actions'];
  public dataSource: Category[] = [];

  constructor(private dialog : MatDialog, private categoryService: CategoryService,
                                private snackBarService: SnackBarService){}

  ngOnInit(): void {
    this.loadAllCategories();
  }

  private loadAllCategories(){
    this.categoryService.getAllCategories()
      .subscribe(
         (resp: Category[]) => {
          this.dataSource = resp;
          }
        )

  }

  public editCategory(inputCategoria : Category){

    this.dialog.open(CategoryEditComponent , {disableClose: true,
      data : { editableCategory : inputCategoria }}).afterClosed().subscribe(
      resp => {
      if (resp) {
          this.loadAllCategories();
          this.snackBarService.showSnackBar('Categoria alterada com sucesso.', 'OK')
        }

      }
    )

  }

  public deleteCategory(categoria : Category){

    this.dialog.open(DialogComponent , {disableClose: true,
      data : {dialogMsg: `Deseja Excluir a categoria `+ categoria.name + `?`, leftButtonLabel: 'Desistir', rightButtonLabel: 'Confirma'}})
      .afterClosed().subscribe(resp => {

        if(resp) {

          this.categoryService.deleteCategory(categoria.guid).subscribe(
            (resp: any) => {
              this.loadAllCategories();
              this.snackBarService.showSnackBar('Categoria apagada com sucesso.', 'OK');

            }, (err: any) => {

              this.snackBarService.showSnackBar('Ocorreu um erro ao apagar a categoria. Categoria em uso', 'OK');

            });
        }
      }
    )

  }

  public createNewCategory(){

    this.dialog.open(CategoryEditComponent , {disableClose: true,
      data : { actionName: 'Criar' }}).afterClosed().subscribe(
      resp => {

        if (resp){

          this.loadAllCategories();
          this.snackBarService.showSnackBar('Categoria criada com sucesso.', 'OK')
        }

      }
    )
  }

}
