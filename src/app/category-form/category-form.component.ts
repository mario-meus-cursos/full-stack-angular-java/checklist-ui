import { CategoryService } from './../services/category.service';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { Category } from '../_models/category';
import { SnackBarService } from '../services/snack-bar.service';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.css']
})
export class CategoryFormComponent implements OnInit{

  @Input() public actionName! :string;

  public categoryForm! : FormGroup;

  @Input() public editableCategory!: Category;

  @Output() closeModelEventEmitter: EventEmitter<boolean> = new EventEmitter<boolean>;

  @ViewChild(`categoryFormDirective`) public categoryFormDirective!: FormGroupDirective;

  public isFormReady = false;

  constructor(private formBuilder: FormBuilder,
    private categoryService: CategoryService,
    private snackBarService: SnackBarService){

  }

  ngOnInit(): void {
    this.categoryForm = this.formBuilder.group({
        name: [this.editableCategory != null ? this.editableCategory.name : '', Validators.required]
    });

    this.isFormReady = true;
  }

  public cancel(){
    this.closeModelEventEmitter.emit(false);
  }

  public save(){
    if (this.categoryForm.valid){

      if (this.actionName == 'Editar'){
        var updateCategory = {
          guid: this.editableCategory.guid,
          name: this.categoryForm.value['name']
        };
        console.log("comp -> " + JSON.stringify(updateCategory));
        this.categoryService.updateCategory(updateCategory)
        .subscribe(
          (resp: any) => {
            this.closeModelEventEmitter.emit(true);
            this.clearForm();

          }, (err: any) =>{
            this.snackBarService.showSnackBar("Não foi possivel atualizar a categoria. Tente novamente. ", "OK");
          }
        );
      } else{

        this.categoryService.saveCategory(this.categoryForm.value['name'])
        .subscribe(
          (resp: any) => {
            this.closeModelEventEmitter.emit(true);
            this.clearForm();

          }, (err: any) =>{
            this.snackBarService.showSnackBar("Não foi possivel criar a categoria. Tente novamente.", "OK");
          }
        );
      }

    }

  }

  public clearForm(){
    this.categoryForm.reset();
    this.categoryFormDirective.resetForm();
  }

}
