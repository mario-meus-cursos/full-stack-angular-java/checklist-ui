import { Component, Inject } from '@angular/core';
import { CheckListItem } from '../_models/checklist_item';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-checklist-edit',
  templateUrl: './checklist-edit.component.html',
  styleUrls: ['./checklist-edit.component.css']
})
export class ChecklistEditComponent {

  public editableCheckList!: CheckListItem;
  public actionName: string = 'Editar';

  constructor(public dialogRef: MatDialogRef<ChecklistEditComponent>,  @Inject(MAT_DIALOG_DATA) dialogData: any ){

      if(dialogData.editableCheckList != null){
        this.editableCheckList = dialogData.editableCheckList;
      }

      if(dialogData.actionName != null){
        this.actionName = dialogData.actionName;
      }
  }



  public onFormClose($event: any){

    if($event != null) this.dialogRef.close($event);

  }


}
